const webpack = require('webpack');

/*
 * Webpack Plugins
 */
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ManifestRevisionPlugin = require('manifest-revision-webpack-plugin');

// take debug mode from the environment
const debug = (process.env.NODE_ENV !== 'production');

// Development asset host (webpack dev server)
const publicHost = debug ? 'http://smbrewing.local:2992' : '';

const rootAssetPath = './assets/';

module.exports = {
  // configuration
  context: __dirname,
  entry: {
    main_js: './assets/js/main',
    main_css: [
      './node_modules/font-awesome/css/font-awesome.css',
    //   './node_modules/bootstrap/dist/css/bootstrap.css',
      './node_modules/bootswatch/paper/custom.css',
      './assets/css/style.css'
    ],
    mgmt_css: [
      './node_modules/bootstrap/dist/css/bootstrap.css',
      './node_modules/gentelella/build/css/custom.css',
      './node_modules/font-awesome/css/font-awesome.css',
      './node_modules/gentelella/vendors/bootstrap-wysiwyg/css/style.css',
      './node_modules/gentelella/vendors/iCheck/skins/flat/green.css'
    ],
    mgmt_js: [
      './node_modules/gentelella/build/js/custom.js',
      './node_modules/gentelella/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js',
      './node_modules/gentelella/vendors/jquery.hotkeys/jquery.hotkeys.js',
      './node_modules/gentelella/vendors/iCheck/icheck.min.js',
      './node_modules/gentelella/vendors/google-code-prettify/src/prettify.js',
      './assets/js/main'
  ],
  },
  output: {
    path: __dirname + '/smbrewing/static/build',
    publicPath: publicHost + '/static/build/',
    filename: '[name].[hash].js',
    chunkFilename: '[id].[hash].js'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.css']
  },
  devtool: debug ? 'inline-sourcemap' : 'source-map',
  devServer: {
    headers: { 'Access-Control-Allow-Origin': '*' }
  },
  module: {
    loaders: [
      { test: /\.html$/, loader: 'raw-loader' },
      { test: /\.less$/, loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader!less-loader' }) },
      { test: /\.css$/, loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader' }) },
      { test: require.resolve('./node_modules/gentelella/vendors/iCheck/icheck.min.js'), loaders: ['expose-loader?iCheck'] },
      { test: require.resolve('jquery'), loaders: ['expose-loader?$', 'expose-loader?jQuery'] },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff' },
      { test: /\.(ttf|eot|svg|png|jpe?g|gif|ico)(\?.*)?$/i,
        loader: 'file-loader?context=' + rootAssetPath + '&name=[path][name].[hash].[ext]' },
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader', query: { presets: ['es2015'], cacheDirectory: true } },
    ]
  },
  plugins: [
    new ExtractTextPlugin('[name].[hash].css'),
    new webpack.ProvidePlugin({ $: 'jquery',
                                'window.$': 'jquery',
                                jQuery: 'jquery',
                                'window.jQuery': 'jquery',
                                iCheck: 'icheck' }),
    new ManifestRevisionPlugin(__dirname + '/smbrewing/webpack/manifest.json', {
      rootAssetPath,
      ignorePaths: ['/js', '/css']
    }),
  ].concat(debug ? [] : [
    // production webpack plugins go here
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
  ])
};
