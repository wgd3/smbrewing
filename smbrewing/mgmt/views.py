# -*- coding: utf-8 -*-
"""Mgmt views."""
from flask import Blueprint, render_template, request, url_for, redirect, flash, session, current_app
from flask_login import current_user

from smbrewing.user.models import User
from smbrewing.blog.models import Post, Category
from .models import *

blueprint = Blueprint('mgmt', __name__, static_folder='./static',
                      subdomain='mgmt',
                      template_folder='templates')

@blueprint.route('/')
def index():

    users = [u for u in User.query.all()]
    return render_template('mgmt_index.html', users=users)

@blueprint.route('/blog/archive')
def blog_archive():
    current_app.logger.debug("Loading blog archive")
    posts = Post.query.all()
    categories = Category.query.all()

    return render_template('mgmt_blog_archive.html', posts=posts, categories=categories)

@blueprint.route('/blog/new')
def new_post():

    return render_template('mgmt_create_post.html')

@blueprint.route('/calendar')
def calendar():

    return render_template('mgmt_calendar.html')

@blueprint.route('/minutes')
def minutes():

    meetings = Meeting.query.all()
    return render_template('mgmt_minutes.html', meetings=meetings)
