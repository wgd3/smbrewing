# -*- coding: utf-8 -*-
"""Management models."""
import datetime as dt
import enum

from flask import current_app
from flask_login import UserMixin
from flask_restful import fields

from sqlalchemy import event
from smbrewing.database import Column, Model, SurrogatePK, db, reference_col, relationship

class Meeting(SurrogatePK, Model):
    """Model for tracking meetings among staff

    """

    __tablename__ = 'meetings'
    date = Column(db.Date, nullable=False, default=dt.datetime.utcnow)
    location = Column(db.String(128))

    def __init__(self, date=None, **kwargs):
        db.Model.__init__(self, date=date, **kwargs)

    def __repr__(self):
        return '<Meeting on {0}>'.format(self.date)

    def __unicode__(self):
        return '{0} Meeting'.format(self.date.strftime('%m/%d/%Y'))


class MeetingNote(SurrogatePK, Model):
    """Model for tracking meeting notes

    """

    __tablename__ = 'meetingnotes'
    date = Column(db.Date, nullable=False, default=dt.datetime.utcnow)
    user_id = reference_col('users')
    user = relationship('User', backref='meetingnotes')
    meeting_id = reference_col('meetings', nullable=False)
    meeting = relationship('Meeting', backref='notes')
    text = Column(db.Text, nullable=False)

    def __init__(self, date=None, meeting_id=None,
                 text='', **kwargs):
        db.Model.__init__(self, date=date, meeting_id=meeting_id,
                          text=text, **kwargs)

    def __repr__(self):
        return '<Note {0} for meeting {1}>'.format(self.id, self.meeting_id)

    def __unicode__(self):
        return 'Note for meeting {0}'.format(self.meeting_id)

class GoalStatusEnum(enum.Enum):
    not_started = "Not Started"
    in_progress = "In Progress"
    finished = "Finished"
    rejected = "Rejected"

class MeetingGoal(SurrogatePK, Model):
    """Model for tracking goals from each meeting

    """

    __tablename__ = 'meetinggoals'
    status = Column(db.Enum(GoalStatusEnum), default=GoalStatusEnum.not_started)
    done = Column(db.Boolean, default=False)
    description = Column(db.Text)
    finished_notes = Column(db.Text)
    meeting_id = reference_col('meetings')
    meeting = relationship('Meeting', backref='goals')
    user_id = reference_col('users')
    assigned_to = relationship('User', backref='goals')
    created_on = Column(db.Date, default=dt.datetime.utcnow)
    due_date = Column(db.Date)
    finished_on = Column(db.Date)

    def __init__(self, status=None, done=False, description='',
                 finished_notes='', **kwargs):
        db.Model.__init__(self, status=status, done=done, description=description,
                          finished_notes=finished_notes, **kwargs)

    def __repr__(self):
        return '<Goal from meeting {0}>'.format(self.meeting_id)

    def __unicode__(self):
        return 'Goal {0} for {1} from meeting {2}'.format(self.id, self.assigned_to, self.meeting_id)

    @staticmethod
    def __json__():
        _json = {
            'id': fields.Integer,
            'status': fields.String,
            'done': fields.Boolean,
            'description': fields.String,
            'meeting_id': fields.Integer,
            'assigned_to': fields.String
        }

        return _json

    def markGoalFinished(self):
        # current_app.logger.debug("Marking goal as finished..")
        self.done = True
        self.finished_on = dt.datetime.utcnow().strftime('%Y-%m-%d')
        # current_app.logger.debug("Done={0} and finished_on={1}".format(self.done, self.finished_on))
        super(MeetingGoal, self).save()
        # current_app.logger.debug("Done={0} and finished_on={1}".format(self.done, self.finished_on))
        return True

    def markGoalUnfinished(self):
        self.done = False
        self.finished_on = None
        return True

class MeetingAttendee(SurrogatePK, Model):
    """Model for tracking who attended what meeting
    """

    __tablename__ = 'meetingattendees'
    user_id = reference_col('users', nullable=False)
    user = relationship('User', backref='meetings_attended')
    meeting_id = reference_col('meetings', nullable=False)
    meeting = relationship('Meeting', backref='attendees')

    def __init__(self, user_id=None, meeting_id=None, **kwargs):
        db.Model.__init__(self, user_id=user_id, meeting_id=meeting_id, **kwargs)

    def __repr__(self):
        return '<User {0} at meeting {1}>'.format(self.user, self.meeting)

    def __unicode__(self):
        return 'User {0} at meeting {1}'.format(self.user, self.meeting)
