# -*- coding: utf-8 -*-
"""Inventory views."""
from flask import Blueprint, render_template, request, url_for, redirect, flash
from flask_login import login_required, current_user, user_unauthorized

from .models import *

import json
import datetime as dt

blueprint = Blueprint('inventory', __name__, url_prefix='/inventory', static_folder='../static')

@blueprint.app_template_filter()
def unitType(unit):
    types = {
        'hopunits': 'Hop',
        'yeastunits': 'Yeast',
        'fermentableunits': 'Fermentable',
        'additiveunits': 'Additive'
    }
    return types[unit]

@blueprint.app_template_filter()
def daysLeft(date):
    today = dt.datetime.now().date()
    expires = date
    diff = expires - today
    return diff.days

@blueprint.route('/')
def index():

    today = dt.datetime.today()
    expireDate = today + dt.timedelta(days=14)
    soonExpire = {}
    soonExpire['hops'] = [h for h in HopUnit.query.filter(HopUnit.expiration_date <= expireDate)]
    soonExpire['fermentables'] = [f for f in FermentableUnit.query.filter(FermentableUnit.expiration_date <= expireDate)]
    soonExpire['yeast'] = [y for y in YeastUnit.query.filter(YeastUnit.expiration_date <= expireDate)]
    soonExpire['additives'] = [a for a in AdditiveUnit.query.filter(AdditiveUnit.expiration_date <= expireDate)]

    mostInventory = {}
    mostInventory['hops'] = [h for h in Hop.query.order_by(Hop.quantity.desc()).limit(5).all()]
    for hop in mostInventory['hops']:
        current_app.logger.debug("{0} hop quantity: {1}".format(hop.name, hop.quantity))
        for unit in hop.hopunits:
            current_app.logger.debug("{0}oz of this hop were bought on receipt {1}".format(unit.quantity, unit.receipt_id))
            current_app.logger.debug("Type of number: " + str(unit.quantity.__class__))

    mostInventory['fermentables'] = [f for f in Fermentable.query.order_by(Fermentable.quantity.desc()).limit(5).all()]

    return render_template('inventory/overview.html', soonExpire=soonExpire, mostInventory=mostInventory)

@blueprint.route('/purchases')
@blueprint.route('/purchases/<int:id>')
def lookupReceipt(id=None):
    if id:
        receipt = Receipt.get_by_id(id)
    else:
        receipt = Receipt.query.all()[0]

    receipts = Receipt.query.all()

    return render_template('inventory/receipt.html', receipt=receipt, receipts=receipts)

@blueprint.route('/new/purchase')
def recordPurchase():

    suppliers = Supplier.query.distinct(InventoryUnit.supplier)
    receipt = Receipt.query.all()[0]

    return render_template('/inventory/purchase.html', suppliers=suppliers, receipt=receipt)
