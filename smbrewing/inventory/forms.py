# -*- coding: utf-8 -*-
"""Inventory forms."""
from flask_wtf import Form
from wtforms import StringField, IntegerField, SelectField, DateField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import DataRequired, Email, EqualTo, Length
