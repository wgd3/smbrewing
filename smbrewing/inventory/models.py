# -*- coding: utf-8 -*-
"""Inventory models."""
import datetime as dt
import enum
import json

from smbrewing.database import Column, Model, SurrogatePK, db, reference_col, relationship

from sqlalchemy import event, func, select
from sqlalchemy.sql import text
from sqlalchemy.ext.hybrid import hybrid_property, Comparator
from sqlalchemy.ext.declarative import declared_attr
from flask import current_app
from flask_restful import fields

class HybridQuantityComparator(Comparator):

    def operate(self, op, other):
        return op(self, other)

    def __eq__(self, other):
        return self == other

    def __lt__(self, other):
        return self < other

    def __gt__(self, other):
        return self > other

class Receipt(SurrogatePK, Model):
    """Model to track purchase receipts
    """
    __tablename__ = 'receipts'
    date = Column(db.Date, nullable=False, default=dt.datetime.utcnow)
    supplier = relationship('Supplier', backref='receipts')
    supplier_id = reference_col('suppliers')
    subtotal = Column(db.Float, nullable=False)
    tax = Column(db.Float, nullable=False)
    total = Column(db.Float)
    purchased_by = relationship('User', backref='receipts')
    purchaser_id = reference_col('users')

    def __init__(self, date=None, **kwargs):
        db.Model.__init__(self, date=date, **kwargs)

    def __repr__(self):
        return '<Receipt from {0}>'.format(self.date)

    def __unicode__(self):
        return '${0} receipt on {1}'.format(round(self.total,2), self.date)

    @hybrid_property
    def subtotal(self):
        if self.line_items:
            return float(sum([l.purchase_price for l in self.line_items]))
        else:
            return float(0)

    @hybrid_property
    def tax(self):
        if self.subtotal > 0:
            return float(self.subtotal * .0675)
        else:
            return float(0)

    @hybrid_property
    def total(self):
        return float(self.subtotal + self.tax)


    @hybrid_property
    def lineItemCount(self):
        count = len(self.hopunits) + len(self.yeastunits)

class Supplier(SurrogatePK, Model):
    """Model for storing a list of suppliers"""
    __tablename__ = 'suppliers'
    name = Column(db.String(128), nullable=False)

    def __init__(self, name='', **kwargs):
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<Supplier {0}>'.format(self.name)

    def __unicode__(self):
        return self.name

class InventoryUnit(Model):
    """Mixin for inventory units
    """
    # __abstract__ = True
    __tablename__ = 'inventoryunits'
    id = Column(db.Integer, primary_key=True)
    quantity = Column(db.Float, nullable=False, default=0.0)
    expired = Column(db.Boolean, nullable=False, default=False)
    purchase_date = Column(db.Date, nullable=False, default=dt.datetime.today)
    expiration_date = Column(db.Date, nullable=True)
    purchase_price = Column(db.Float, nullable=False)
    supplier = Column(db.String(128))
    type = Column(db.String(128))
    __mapper_args__ = {
        'polymorphic_on': type,
    }

    @declared_attr
    def receipt(cls):
        return relationship('Receipt', backref='line_items')

    @declared_attr
    def receipt_id(cls):
        return Column(db.Integer, db.ForeignKey('receipts.id'))

    def getAveragePricePerUnit(self):
        return self.quantity / self.purchase_date

class Hop(SurrogatePK, Model):

    __tablename__ = 'hops'
    name = Column(db.String(80), nullable=False)
    form = Column(db.String(80), nullable=False)
    aa = Column(db.Float)
    quant_available = Column(db.Float)
    in_stock = Column(db.Boolean(), default=True)
    description = Column(db.Text, default='This hop needs a description!')

    def __init__(self, name='', form='', **kwargs):
        db.Model.__init__(self, name=name, form=form, **kwargs)

    def __repr__(self):
        return '<Hop {0}>'.format(self.name)

    def __unicode__(self):
        return self.name

    @hybrid_property
    def quantity(self):
        return float(sum([h.quantity for h in self.hopunits]))

    @quantity.expression
    def quantity(cls):
        return select([func.sum(HopUnit.quantity)]).\
                where(HopUnit.id==cls.id).\
                label('total_quantity')

    # @quantity.comparator
    # def quantity(cls):
    #     return Hop.HybridQuantityComparator(cls.quantity)

    @staticmethod
    def __json__():
        _json = {
            'id': fields.Integer,
            'name': fields.String,
            'form': fields.String,
            'aa': fields.Float,
            'quantity': fields.Float,
            'in_stock': fields.Boolean,
            'description': fields.String
        }

        return _json

class HopUnit(InventoryUnit):
    """Model for tracking individual hop purchases
    """
    __tablename__ = 'hopunits'
    __table_args__ = {'useexisting': True}
    id = Column(db.Integer, db.ForeignKey('inventoryunits.id'), primary_key=True)
    name = relationship('Hop', backref='hopunits')
    hop_id = reference_col('hops')
    __mapper_args__ = {
        'polymorphic_identity':'hopunits',
    }

    def __init__(self, **kwargs):
        super(HopUnit, self).__init__(**kwargs)
        cls_ = type(self)
        for k in kwargs:
            if not hasattr(cls_, k):
                raise TypeError("{0} is an invalid keyword argument for {1}".format(k, cls_.__class__))
            setattr(self, k, kwargs[k])

class Yeast(SurrogatePK, Model):

    __tablename__ = 'yeasts'
    name = Column(db.String(80), unique=True, nullable=False)
    lab = Column(db.String(80))
    product = Column(db.String(80))
    attenuation = Column(db.Float)
    flocculation = Column(db.String(80))
    min_temp = Column(db.Integer)
    max_temp = Column(db.Integer)
    quant_available = Column(db.Float)
    in_stock = Column(db.Boolean(), default=True)
    form = Column(db.String(64))
    description = Column(db.Text)

    def __init__(self, name='', **kwargs):
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<Yeast {0}>'.format(self.name)

    def __unicode__(self):
        return '{0} - {1}'.format(self.name, self.product)

class YeastUnit(InventoryUnit):
    """Model for tracking yeast line items on receipts"""
    __tablename__ = 'yeastunits'
    __table_args__ = {'useexisting': True}
    id = Column(db.Integer, db.ForeignKey('inventoryunits.id'), primary_key=True)
    name = relationship('Yeast', backref='yeastunits')
    yeast_id = reference_col('yeasts')
    __mapper_args__ = {
        'polymorphic_identity':'yeastunits',
    }

    def __init__(self, **kwargs):
        super(YeastUnit, self).__init__(**kwargs)
        cls_ = type(self)
        for k in kwargs:
            if not hasattr(cls_, k):
                raise TypeError("{0} is an invalid keyword argument for {1}".format(k, cls_.__class__))
            current_app.logger.debug("Setting key {0} to value {1}".format(k, kwargs[k]))
            setattr(self, k, kwargs[k])

class Fermentable(SurrogatePK, Model):
    """Small records for barley, etc"""

    __tablename__ = 'fermentables'
    name = Column(db.String(128), unique=True, nullable=False)
    description = Column(db.Text)
    color = Column(db.Integer)
    quant_available = Column(db.Float)
    in_stock = Column(db.Boolean(), default=True)
    mash_max = Column(db.Float)
    potential_sg = Column(db.Float)
    origin = Column(db.String(64))
    ferm_type = Column(db.String(64))

    def __init__(self, name='', **kwargs):
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<Fermentable {0}>'.format(self.name)

    def __unicode__(self):
        return self.name

    @hybrid_property
    def quantity(self):
        return sum([f.quantity for f in self.fermentableunits])

    @quantity.expression
    def quantity(cls):
        return select([func.sum(FermentableUnit.quantity)]).\
                where(FermentableUnit.id==cls.id).\
                label('total_quantity')

    @staticmethod
    def __json__(group=None):
        _json = {
            'id': fields.Integer,
            'name': fields.String,
            'color': fields.Integer,
            'description': fields.String,
            'quant_available': fields.Float,
            'in_stock': fields.Boolean
        }

        return _json

class FermentableUnit(InventoryUnit):
    """Model for tracking yeast line items on receipts"""
    __tablename__ = 'fermentableunits'
    __table_args__ = {'useexisting': True}
    id = Column(db.Integer, db.ForeignKey('inventoryunits.id'), primary_key=True)
    name = relationship('Fermentable', backref='fermentableunits')
    fermentable_id = reference_col('fermentables')
    __mapper_args__ = {
        'polymorphic_identity':'fermentableunits',
    }

    def __init__(self, **kwargs):
        super(FermentableUnit, self).__init__(**kwargs)
        cls_ = type(self)
        for k in kwargs:
            if not hasattr(cls_, k):
                raise TypeError("{0} is an invalid keyword argument for {1}".format(k, cls_.__class__))
            current_app.logger.debug("Setting key {0} to value {1}".format(k, kwargs[k]))
            setattr(self, k, kwargs[k])

class Additive(SurrogatePK, Model):
    """Class for non-fermenting additives
    """
    __tablename__ = 'additives'
    name = Column(db.String(128), nullable=False)
    quant_available = Column(db.Float)
    in_stock = Column(db.Boolean)

    def __init__(self, name='', **kwargs):
        if kwargs.has_key('quant_available'):
            if kwargs.get('quant_available') > 0:
                self.in_stock = True
                current_app.logger.debug("Adding {0} and marking in-stock".format(self.name))
            else:
                self.in_stock = False
                current_app.logger.debug("Adding {0} with no inventory".format(self.name))
        else:
            self.in_stock = False

        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<Additive {0}>'.format(self.name)

    def __unicode__(self):
        return self.name

    @staticmethod
    def __json__(group=None):
        _json = {
            'name': fields.String,
            'quant_available': fields.Float,
            'in_stock': fields.Boolean
        }

        return _json

class AdditiveUnit(InventoryUnit):
    """Model for tracking yeast line items on receipts"""
    __tablename__ = 'additiveunits'
    __table_args__ = {'useexisting': True}
    id = Column(db.Integer, db.ForeignKey('inventoryunits.id'), primary_key=True)
    name = relationship('Additive', backref='additiveunits')
    additive_id = reference_col('additives')
    __mapper_args__ = {
        'polymorphic_identity':'additiveunits',
    }

    def __init__(self, **kwargs):
        super(AdditiveUnit, self).__init__(**kwargs)
        cls_ = type(self)
        for k in kwargs:
            if not hasattr(cls_, k):
                raise TypeError("{0} is an invalid keyword argument for {1}".format(k, cls_.__class__))
            current_app.logger.debug("Setting key {0} to value {1}".format(k, kwargs[k]))
            setattr(self, k, kwargs[k])

class VesselTypes(enum.Enum):
    mlt = "Mash Lauter Tun"
    kettle = "Brew Kettle"
    carboy = "Fermenter - Carboy"
    bucket = "Fermenter - Bucket"
    conical = "Fermenter - Conical"

class Vessel(SurrogatePK, Model):
    """Class for tracking equipment such as brew kettles, fermenters, etc

    """

    __tablename__ = 'vessels'
    size = Column(db.Float, nullable=False)
    vessel_type = Column(db.Enum(VesselTypes))
    nickname = Column(db.String(128))

    def __init__(self, size=None, **kwargs):
        db.Model.__init__(self, size=size, **kwargs)

def after_create_add_supplier(mapper, connection, target):
    """Function for adding a new supplier to suppliers table if
    it doesn't exist
    """
    cur_table = target.__table__
    sup_table = Supplier.__table__

    # current_app.logger.debug("Checking for presence of supplier " + target.supplier)
    if target.supplier not in [s.name for s in Supplier.query.all()]:
        print "DEBUG before_create: Adding supplier"
        ins = sup_table.insert()
        connection.execute(ins, name=target.supplier)
        # supplier = Supplier(name=target.supplier)
        # supplier.save()
        # current_app.logger.debug("Saved new supplier!")



def after_update_check_stock(mapper, connection, target):
    """Function for auto-updating the 'in_stock' boolean

    All items that track inventory have 'name', 'quant_available'
    and 'in_stock' fields
    """
    cur_table = target.__table__

    print "Checking stock of {0}".format(target.name)
    # print "DEBUG: {0}".format(target.__table__)
    if target.quant_available <= 0:
        print "{0} is no longer in stock".format(target.name)
        connection.execute(
            cur_table.update().
            where(cur_table.c.id == target.id).
            values(in_stock=False)
        )
    else:
        print "{0} is still in stock".format(target.name)
        connection.execute(
            cur_table.update().
            where(cur_table.c.id == target.id).
            values(in_stock=True)
        )


event.listen(Fermentable, 'after_update', after_update_check_stock)
event.listen(Hop, 'after_update', after_update_check_stock)
event.listen(Yeast, 'after_update', after_update_check_stock)
event.listen(Additive, 'after_update', after_update_check_stock)
event.listen(HopUnit, 'before_insert', after_create_add_supplier)
