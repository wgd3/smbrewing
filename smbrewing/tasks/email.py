from flask import current_app, render_template
from flask_mail import Message

from smbrewing.extensions import mail
from .celery_worker import celery

@celery.task
def send_async_email(msg_dict):
    msg = Message()
    msg.__dict__.update(msg_dict)
    mail.send(msg)

def msg_to_dict(to, subject, template, **kwargs):
    msg = Message(
        subject=subject,
        sender=current_app.config['MAIL_DEFAULT_SENDER'],
        recipients=[to]
    )
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    return msg.__dict__

def send_email(to, subject, template, **kwargs):
    # current_app.logger.debug("DEBUG - Sending message to {0} about {1}".format(to, subject))
    send_async_email.delay(msg_to_dict(to, subject, template, **kwargs))
