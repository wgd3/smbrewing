#!/usr/bin/env python

import os
from smbrewing.app import create_app, make_celery

app = create_app()
celery = make_celery(app)
# app.app_context().push()
