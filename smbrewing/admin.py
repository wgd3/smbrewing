# -*- coding: utf-8 -*-
"""admin module views"""

from flask_admin import BaseView, expose, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user
from flask import current_app

from smbrewing.extensions import db, admin
from smbrewing.user.models import User, Role
from smbrewing.blog.models import Post, Category
from smbrewing.beers.models import Beer
from smbrewing.inventory.models import Hop, Fermentable, Yeast, Additive

### Class for pointing flask-admin towards the mgmt module
class MgmtHomeView(AdminIndexView):
    @expose('/')
    def index(self):
        pass

class AdminModelView(ModelView):

    def is_accessible(self):
        return current_user.is_admin

    list_template = 'admin_list.html'
    create_template = 'admin_create.html'
    edit_template = 'admin_edit.html'

class UserModelView(AdminModelView):
    column_exclude_list = ['password']

    def on_model_change(self, form, model, is_created):
        if is_created:
            current_app.logger.debug("Modifying user model before submitting to db")
            model.username = form.username.data
            model.email = form.email.data
            model.created_at = form.created_at.data
            model.first_name = form.first_name.data
            model.last_name = form.last_name.data
            model.active = form.active.data
            model.set_password(form.password.data)

class RoleModelView(AdminModelView):
    column_exclude_list = ['user']
    # form_excluded_columns = ['user']

class BlogPostModelView(AdminModelView):

    column_exclude_list = ['slug', 'content']

    def on_model_change(self, form, instance):
        instance.save()

class BeerModelView(AdminModelView):

    def _format_abv(v, c, m, n):
        return "{0}%".format(m.abv)

    column_formatters = dict(
        abv=_format_abv
    )

    column_labels = {
        'abv': 'ABV',
        'ibu': 'IBU'
    }

class HopModelView(AdminModelView):

    def _display_form(v, c, m, n):
        return m.form.capitalize()

    def _display_aa(v, c, m, n):
        return "{0}%".format(m.aa)

    column_labels = {
        'aa': 'Alpha Acid'
    }
    column_exclude_list = ['quant_available']
    column_formatters = dict(
        form=_display_form,
        aa=_display_aa
    )
class FermentableModelView(AdminModelView):

    def _display_mash_percentage(v, c, m ,n):
        if m.mash_max:
            return "{0}%".format(m.mash_max*100)

    column_formatters = dict(
        mash_max=_display_mash_percentage
    )
    column_labels = {
        'mash_max': 'Max In Mashbill',
        'potential_sg': 'Potential SG'
    }
    column_exclude_list = ['description', 'quant_available']

class YeastModelView(AdminModelView):
    pass

### Adding Flask-Admin to Management module
admin.base_template = 'mgmt_layout.html'

### Adding views to admin object
admin.add_view(UserModelView(User, db.session, name='Users', endpoint='useradmin', category='User Management'))
admin.add_view(RoleModelView(Role, db.session, name='Roles', endpoint='roleadmin', category='User Management'))

admin.add_view(BlogPostModelView(Post, db.session, name='Blog Posts', endpoint='blogposts', category='Blog'))

admin.add_view(BeerModelView(Beer, db.session, name='Beer', endpoint='beeradmin', category='Beer'))

admin.add_view(HopModelView(Hop, db.session, name='Hops', endpoint='hopadmin', category='Inventory'))
admin.add_view(FermentableModelView(Fermentable, db.session, name='Fermentables', category='Inventory'))
admin.add_view(YeastModelView(Yeast, db.session, name='Yeast', category='Inventory'))
