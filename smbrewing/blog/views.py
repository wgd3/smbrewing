# -*- coding: utf-8 -*-
"""Blog views."""
from flask import Blueprint, render_template, current_app, flash, redirect, url_for, request
from flask_login import login_required, current_user

from .models import Post, Category

from smbrewing.utils import flash_errors

blueprint = Blueprint('blog', __name__, url_prefix='/blog', static_folder='../static')

@blueprint.app_template_filter()
def monthText(month):
    months = {
        1: 'January',
        2: 'February',
        3: 'March',
        4: 'April',
        5: 'May',
        6: 'June',
        7: 'July',
        8: 'August',
        9: 'September',
        10: 'October',
        11: 'November',
        12: 'December'
    }
    return months[month]

def get_post_archive():

    # TODO - any time get_post_archive is returned with a template, it's a second
    # db query. Might try to find a better way to do this
    ym_list = [(p.timestamp.year, p.timestamp.month) for p in Post.query.all()]
    date_list = {}
    for ym in ym_list:
        if not date_list.has_key(ym[0]): # check for year
            date_list[ym[0]] = {} #init empty list for that year

        if not date_list[ym[0]].has_key(ym[1]):
            date_list[ym[0]][ym[1]] = 0

        date_list[ym[0]][ym[1]] += 1

    return date_list

@blueprint.route('/')
def index():

    posts = Post.query.filter(Post.published == True).all()
    current_app.logger.debug("Found {0} blog posts in the database".format(len(posts)))

    return render_template('blog/index.html', posts=posts, blog_history=get_post_archive())

@blueprint.route('/<string:slug>')
def blogPost(slug):

    post = Post.query.filter(Post.slug == slug).first()

    return render_template('blog/post.html', post=post, blog_history=get_post_archive())

@blueprint.route('/archive')
def blogArchives():
    pass

@blueprint.route('/archive/<int:year>/<int:month>')
def archiveMonth(year, month):

    posts = Post.query.all()
    archivePosts = []
    for p in posts:
        if p.timestamp.year == year and p.timestamp.month == month:
            archivePosts.append(p)

    return render_template('blog/archiveMonth.html', posts=archivePosts,
                           blog_history=get_post_archive(), year=year,
                           month=month)
