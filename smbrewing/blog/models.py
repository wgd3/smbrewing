# -*- coding: utf-8 -*-
"""Beer models."""
import datetime as dt
import pytz
import enum
import re

from smbrewing.database import Column, Model, SurrogatePK, db, reference_col, relationship

from sqlalchemy import event
from flask import current_app
from flask_restful import fields

class Post(SurrogatePK, Model):
    """Model for blog posts

    """

    __tablename__ = 'blog_posts'
    timestamp = Column(db.DateTime, default=dt.datetime.utcnow)
    content = Column(db.Text, nullable=False)
    published = Column(db.Boolean, default=True)
    title = Column(db.String(140), nullable=False)
    slug = Column(db.String(140))
    user_id = reference_col('users')
    user = relationship('User', backref='blog_posts')

    def save(self, *args, **kwargs):
        if not self.slug:
            current_app.logger.debug("Detected a blog post without a slug - generating")
            self.slug = re.sub('[^\w]+', '-', self.title.lower())
            current_app.logger.debug("Post slug: " + self.slug)

        # update TZ INFO
        # naive_obj = self.timestamp
        # localtz = pytz.timezone("US/Eastern")
        # local_dt = localtz.localize(naive_obj)
        # self.timestamp = local_dt

        return super(Post, self).save()

    def __init__(self, content='', title='', **kwargs):
        db.Model.__init__(self, content=content, title=title, **kwargs)

    def __repr__(self):
        return '<Blog post {0}>'.format(self.id)

    def __unicode__(self):
        return 'BlogPost: {0}'.format(self.title)

    def getMonth(self):
        return self.timestamp.strftime('%B')

    def getLocalTimestamp(self, tz='America/New_York'):
        dt_obj = self.timestamp
        localtz = pytz.timezone(tz)
        return localtz.localize(dt_obj)

class Category(SurrogatePK, Model):
    """Blog post categories

    """

    __tablename__ = 'blog_categories'
    name = Column(db.String(64), nullable=False)

    def __init__(self, name='', **kwargs):
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<Category {0}>'.format(self.name)

    def __unicode__(self):
        return '{0}'.format(self.name)
