# -*- coding: utf-8 -*-
"""Beer views."""
from flask import Blueprint, render_template, request, url_for, redirect, flash, session, current_app
from flask_login import current_user

from .models import Beer

from functools import wraps
import json

blueprint = Blueprint('beers', __name__, url_prefix='/beers', static_folder='../static')

# @blueprint.app_template_filter()
# def splitSteps(recipe):
#     steps_json = recipe.steps
#     return [s for s in steps_json['steps']]

# def login_required(role="ANY"):
#     def wrapper(fn):
#         @wraps(fn)
#         def decorated_view(*args, **kwargs):
#             if not current_user.is_authenticated:
#                 return user_unauthorized
#             user_roles = [r.name for r in current_user.roles]
#             # print "DEBUG: Checking that {0} has the {1} role".format(
#             #     current_user.username,
#             #     role
#             # )
#             if ((role not in user_roles) and (role != "ANY")):
#                 return user_unauthorized
#             return fn(*args, **kwargs)
#         return decorated_view
#     return wrapper

@blueprint.route('/')
def index():
    """Render the beer overview page"""
    beers = Beer.query.all()

    return render_template('beers/beers.html', beers=beers)

# @blueprint.route('/brewday')
# # @login_required(role="brewer")
# def brewday():
#
#     recipes = BeerRecipe.query.all()
#
#     # check session for recipe from previous page visit
#     if session.get('receipe_id'):
#         current_app.logger.debug("Found receipe in session, loading recipe with page")
#         recipe_id = session.get('receipe_id')
#         return render_template('beers/brewday.html', recipes=recipes, recipe_id=receipe_id)
#
#     return render_template('beers/brewday.html', recipes=recipes)
#
#
#
# @blueprint.route('/newrecipe', methods = ['GET'])
# # @login_required(role="brewer")
# def newrecipe():
#     """Route for creating a new recipe from the front endpoint
#
#     """
#
#     yeasts = Yeast.query.all()
#     hops = Hop.query.all()
#     fermentables = Fermentable.query.all()
#     additives = Additive.query.all()
#     load_session_recipe = 'false'
#
#     if session.get('draft_recipe'):
#         load_session_recipe = 'true'
#
#     return render_template('beers/newrecipe.html', yeasts=yeasts,
#                                                    hops=hops,
#                                                    fermentables=fermentables,
#                                                    additives=additives,
#                                                    load_session_recipe=load_session_recipe)
#
# @blueprint.route('/newrecipe/pdf')
# def getRecipePDF():
#
#     return render_pdf(url_for('beers.newrecipe'))
#
# @blueprint.route('/managebrewery')
# # @login_required(role="brewer")
# def manageBrewery():
#
#     fermenters = Vessel.query.filter((Vessel.vessel_type == 'carboy') | (Vessel.vessel_type == 'bucket')).all()
#
#
#     return render_template('beers/managebrewery.html', fermenters=fermenters)
