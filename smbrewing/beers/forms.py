# -*- coding: utf-8 -*-
"""Beer forms."""
# from flask_wtf import Form
# from wtforms import StringField, IntegerField, SelectField, DecimalField
# from wtforms.ext.sqlalchemy.fields import QuerySelectField
# from wtforms.validators import DataRequired, Email, EqualTo, Length
#
# from .models import Beer
#
# class NewRecipeForm(Form):
#     """Create a new recipe"""
#
#     recipe_name = StringField('Recipe Name',
#                               validators=[DataRequired()])
#     batch_size = DecimalField('Batch Size (gal)',
#                               validators=[DataRequired()])
#     method = SelectField('Method',
#                          choices=[('all grain', 'All Grain'),
#                                   ('partial mash', 'Partial Mash'),
#                                   ('biab', 'BIAB'),
#                                   ('extract', 'Extract')],
#                          validators=[DataRequired()])
