# -*- coding: utf-8 -*-
"""Beer models."""
import datetime as dt
import enum
import json
import pytz

from smbrewing.database import Column, Model, SurrogatePK, db, reference_col, relationship

from sqlalchemy import event
from sqlalchemy.ext import mutable
from flask import current_app
from flask_restful import fields

class Beer(SurrogatePK, Model):
    """A class for individual beers"""

    __tablename__ = 'beers'
    name = Column(db.String(80), unique=True, nullable=False, default="NameMe")
    description = Column(db.Text(), default="This beer needs a description!")
    abv = Column(db.Float())
    ibu = Column(db.Integer())
    available = Column(db.Boolean(), default=True)
    seasonal = Column(db.Boolean(), default=False)
    style_id = reference_col('styles')
    style = relationship('BeerStyle', backref='beers')
    recipe = relationship("BeerRecipe", backref='recipes')
    recipe_id = reference_col('recipes')

    def __init__(self, name=None, **kwargs):
        """Create instance"""
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<Beer({name})>'.format(name=self.name)

class BeerRecipeStepStage(enum.Enum):
    mash = "Mash"
    lauter = "Lauter"
    boil = "Boil"
    ferment = "Fermentation"
    condition = "Conditioning"

    def __str__(self):
        return '%s' % self._value_

class BeerRecipeStep(SurrogatePK, Model):
    """Easier than trying to coerce JSON into db columns
    """
    __tablename__ = 'recipesteps'
    recipe_id = reference_col('recipes', nullable=False)
    recipe = relationship('BeerRecipe', backref='steps')
    time = Column(db.Integer)
    description = Column(db.Text, nullable=False)
    temp = Column(db.Float)
    stage = Column(db.Enum(BeerRecipeStepStage))

    def __init__(self, description='', recipe_id=None, **kwargs):
        db.Model.__init__(self, description=description,
                          recipe_id=recipe_id, **kwargs)

    def __repr__(self):
        return '<Step {0} for recipe {1}>'.format(self.id, self.recipe_id)

    @staticmethod
    def __json__(group=None):
        _json = {
            'time': fields.Integer,
            'description': fields.String,
            'temp': fields.Float,
            'stage': fields.String
        }

        if group == 'flat':
            return _json

        return _json

class BeerRecipeHop(SurrogatePK, Model):
    """Tracking which hops are in what recipe
    """
    __tablename__ = 'recipehops'
    recipe_id = reference_col('recipes', nullable=False)
    recipe = relationship('BeerRecipe', backref='hops')
    time = Column(db.Integer)
    hop_id = reference_col('hops')
    name = relationship('Hop', backref='recipes')
    dry_hop = Column(db.Boolean, default=False)
    quantity = Column(db.Float)

    def __init__(self, recipe_id=None, hop_id=None, **kwargs):
        db.Model.__init__(self, recipe_id=recipe_id, hop_id=hop_id, **kwargs)

    def __repr__(self):
        return '<Hop {0} used in recipe {1}>'.format(self.name, self.recipe)

    @staticmethod
    def __json__(group=None):
        _json = {
            'hop_id': fields.Integer,
            'name': fields.String,
            'quantity': fields.Float,
            'time': fields.Integer,
            'dry_hop': fields.Boolean,
        }

        if group == 'flat':
            return _json

        return _json


class BeerRecipeMashbill(SurrogatePK, Model):
    """Tracking all fermentables and additives in recipes
    """
    __tablename__ = 'recipemashbill'
    recipe_id = reference_col('recipes', nullable=False)
    recipe = relationship('BeerRecipe', backref='mashbill')
    fermentable_id = reference_col('fermentables', nullable=False)
    name = relationship('Fermentable', backref='recipes')
    quantity = Column(db.Float)

    def __init__(self, recipe_id=None, fermentable_id=None, **kwargs):
        db.Model.__init__(self, recipe_id=recipe_id,
                          fermentable_id=fermentable_id, **kwargs)

    def __repr__(self):
        return '<Fermentable {0} used in recipe {1}>'.format(self.name, self.recipe)

    @staticmethod
    def __json__(group=None):
        _json = {
            'fermentable_id': fields.Integer,
            'name': fields.String,
            'quantity': fields.Float
        }

        if group == 'flat':
            return _json

        return _json

class BeerRecipeAdditive(SurrogatePK, Model):
    """
    """
    __tablename__ = 'recipeadditives'
    recipe_id = reference_col('recipes', nullable=False)
    recipe = relationship('BeerRecipe', backref='additives')
    additive_id = reference_col('additives', nullable=False)
    name = relationship('Additive', backref='recipes')
    quantity = Column(db.Float)

    def __init__(self, recipe_id=None, additive_id=None, **kwargs):
        db.Model.__init__(self, recipe_id=recipe_id, additive_id=additive_id, **kwargs)

    def __repr__(self):
        return '<Additive {0} used in recipe {1}>'.format(self.name, self.recipe)

    @staticmethod
    def __json__(group=None):
        _json = {
            'additive_id': fields.Integer,
            'name': fields.String,
            'quantity': fields.Float
        }

        return _json


class BeerRecipe(SurrogatePK, Model):
    """For storing beer recipes

    """

    __tablename__ = 'recipes'
    name = Column(db.String(128), unique=True, nullable=False, default="NameMe")
    yeast_id = reference_col('yeasts')
    yeast_name = relationship('Yeast', backref='recipes')
    batch_size = Column(db.Float)
    style_id = reference_col('styles')
    style_name = relationship('BeerStyle', backref='recipes')
    method = Column(db.String(64))
    target_og = Column(db.Float)
    target_fg = Column(db.Float)
    target_srm = Column(db.Integer)
    target_abv = Column(db.Float)
    target_ibu = Column(db.Integer)

    def __init__(self, name='', yeast_id=None, **kwargs):
        db.Model.__init__(self, name=name,
                          yeast_id=yeast_id, **kwargs)

    def __repr__(self):
        return '<BeerRecipe - {0}>'.format(self.name)

    def __unicode__(self):
        return self.name

    @staticmethod
    def __json__(group=None):
        """ Copying from StackOverflow
        https://stackoverflow.com/questions/22035974/flask-restful-marshal-complex-object-to-json
        """
        _json = {
            'id': fields.Integer,
            'name': fields.String,
            'yeast_id': fields.Integer,
            'yeast_name': fields.String,
            'batch_size': fields.Float,
            'style_id': fields.Integer,
            'style_name': fields.String,
            'method': fields.String,
            'target_og': fields.Float,
            'target_fg': fields.Float,
            'target_srm': fields.Float,
            'target_ibu': fields.Float,
            'target_abv': fields.Float,
        }

        if group == 'flat':
            return _json

        _json['hops'] = fields.Nested(BeerRecipeHop.__json__('flat'))
        _json['mashbill'] = fields.Nested(BeerRecipeMashbill.__json__('flat'))
        _json['additives'] = fields.Nested(BeerRecipeAdditive.__json__('flat'))
        _json['steps'] = fields.Nested(BeerRecipeStep.__json__('flat'))
        return _json

    def add_step(self, description, **kwargs):
        step = BeerRecipeStep(description=description, recipe_id=self.id, **kwargs)
        try:
            step.save()
            current_app.logger.debug("Added a new step to recipe {0}".format(self.id))
        except Exception as e:
            current_app.logger.error("Could not save new step for recipe {0}".format(self.id))
            current_app.logger.error(e)
            return False

        return True


class BrewLog(SurrogatePK, Model):
    """Brewing history

    method - ['all grain', 'partial mash', 'BIAB', 'extract']
    """

    __tablename__ = 'brewlogs'
    date = Column(db.Date, nullable=False, default=dt.date.today)
    brewer_id = reference_col('users')
    brewer = relationship('User', backref='brewlogs')
    recipe_id = reference_col('recipes')
    recipe_name = relationship('BeerRecipe', backref='brewlogs')
    batch_size = Column(db.Float)
    method = Column(db.String(80))
    notes = Column(db.Text)

    def __init__(self, date=None, brewer_id=None, recipe_id=None, **kwargs):
        db.Model.__init__(self, date=date, brewer_id=brewer_id, recipe_id=recipe_id, **kwargs)

    def __repr__(self):
        return '<Brewlog for {0} on {1} by {2}>'.format(self.recipe_name,
                                                        self.date,
                                                        self.brewer)

class BJCPCategory(SurrogatePK, Model):
    """BJCP Categories"""

    __tablename__ = 'bjcp_categories'
    bjcp_category_number = Column(db.Integer)
    name = Column(db.String(128), unique=True, nullable=False)

    def __init__(self, name=None, **kwargs):
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<BJCP Style {0}>'.format(self.name)

    def __unicode__(self):
        return 'BJCP Category: ' +self.name


class BeerStyle(SurrogatePK, Model):
    """BJCP Styles"""

    __tablename__ = 'styles'
    name = Column(db.String(128), unique=True, nullable=False)
    bjcp_id = reference_col('bjcp_categories')
    bjcp_category = relationship('BJCPCategory', backref='styles')
    bjcp_letter = Column(db.String(12))
    min_og = Column(db.Float)
    max_og = Column(db.Float)
    min_fg = Column(db.Float)
    max_fg = Column(db.Float)
    min_ibu = Column(db.Integer)
    max_ibu = Column(db.Integer)
    min_abv = Column(db.Float)
    max_abv = Column(db.Float)
    min_srm = Column(db.Integer)
    max_srm = Column(db.Integer)
    aroma = Column(db.Text)
    appearance = Column(db.Text)
    flavor = Column(db.Text)
    mouthfeel = Column(db.Text)
    impression = Column(db.Text)
    comments = Column(db.Text)
    ingredients = Column(db.Text)
    commercial_examples = Column(db.Text)

    def __init__(self, name=None, **kwargs):
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<BeerStyle {0}>'.format(self.name)

    def __unicode__(self):
        return self.name

    def getBJCPStyle(self):
        return self.bjcp_id+self.bjcp_letter

    @staticmethod
    def __json__(extended=False):
        """ Copying from StackOverflow
        https://stackoverflow.com/questions/22035974/flask-restful-marshal-complex-object-to-json
        """
        if extended:
            pass

        _json = {
            'id': fields.Integer,
            'name': fields.String,
            'bjcp_id': fields.Integer,
            'bjcp_category': fields.String,
            'bjcp_letter': fields.String,
        }
        return _json

class BrewTimer(SurrogatePK, Model):
    """Timer for tracking brew days

    Valid states:
    - created (not running)
    - running
    - paused
    - expired
    """

    __tablename__ = 'brewtimers'
    created_at = Column(db.DateTime)
    duration = Column(db.Integer, nullable=False)
    status = Column(db.String(64))
    target_time = Column(db.DateTime)
    paused_at = Column(db.DateTime)

    def __init__(self, created_at=None, duration=10, status='created',
                 target_time=None, **kwargs):
        self.created_at = dt.datetime.now(pytz.timezone('US/Eastern'))
        self.target_time = self.created_at + dt.timedelta(minutes=int(duration))
        current_app.logger.debug("Creating new BrewTimer with target_time {0} and created_time {1}".format(self.target_time, self.created_at))
        db.Model.__init__(self, created_at=self.created_at, duration=duration,
                          status=status, target_time=self.target_time, **kwargs)

    @staticmethod
    def __json__():
        _json = {
            'id': fields.Integer,
            'status': fields.String,
            'created_at': fields.DateTime,
            'target_time': fields.DateTime,
            'duration': fields.Integer
        }

        return _json

    def getStatus(self):
        if self.status == 'paused':
            pass
        if self.status == 'running':
            # check if we're expired first
            if dt.datetime.now() >= self.target_time:
                current_app.logger.debug("Found timer expired, setting status")
                self.status = 'expired'

            remaining = self.target_time - dt.datetime.now()
            current_app.logger.debug('Running getStatus on time, with {0} minutes remaining'.format(remaining))

        return {
            'status': self.status,
            'target_time': self.target_time
        }

    def startTimer(self):
        self.status = 'running'
        self.save()
        return True

    def expireTimer(self):
        self.status = 'expired'
        self.save()
        return True

    def pause(self):
        self.status = 'paused'
        self.paused_at = dt.datetime.now()
        self.save()
        return True

    def resume(self):
        if self.status != 'paused':
            return False

        self.status = 'running'
        paused_for = dt.datetime.now() - self.paused_at
        current_app.logger.debug("Adding {0} minutes to timer now that it's resuming".format(paused_for))
        self.target_time = self.target_time + paused_for
        self.save()

    def __repr__(self):
        return '<BrewTimer {0} currently {1}>'.format(self.id, self.status)
