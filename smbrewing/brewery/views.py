# -*- coding: utf-8 -*-
"""Brewery views."""
from flask import Blueprint, render_template, request, url_for, redirect, flash, session, current_app
from flask_login import current_user

from smbrewing.user.models import User
from smbrewing.blog.models import Post, Category
from smbrewing.beers.models import BeerRecipe, Beer, BeerStyle
from smbrewing.inventory.models import Hop, Fermentable, Yeast, Additive

blueprint = Blueprint('brewery', __name__, static_folder='./static',
                      subdomain='brewery',
                      template_folder='./templates')

@blueprint.route('/')
def index():

    return render_template('brewery_index.html')

@blueprint.route('/recipes')
def recipes():

    recipes = BeerRecipe.query.all()
    return render_template('brewery_recipes.html', recipes=recipes)

@blueprint.route('/beers')
def beers():

    beers = Beer.query.all()
    return render_template('brewery_beers.html', beers=beers)

@blueprint.route('/recipe/new')
def newrecipe():

    styles = BeerStyle.query.all()
    hops = Hop.query.all()
    fermentables = Fermentable.query.all()
    yeasts = Yeast.query.all()
    additives = Additive.query.all()
    return render_template('brewery_newrecipe.html', styles=styles,
                           hops=hops, fermentables=fermentables,
                           yeasts=yeasts, additives=additives)
